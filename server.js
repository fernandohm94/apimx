var express = require('express'),
  jwt = require('jsonwebtoken'),
  config = require('./configs/config'),
  app = express(),
  port = process.env.PORT || 3000;

app.set('llave', config.llave)

var path = require('path');
var requestjson = require('request-json')
var bodyparser = require('body-parser')

app.use(bodyparser.urlencoded({ extended: true }))
app.use(bodyparser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*")
  //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  //res.header("Access-Control-Allow-Credentials", "true")
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
  res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Access-Token")
  next();
})

var movimientosJSON = require('./movimientosv2.json')

var urlClientes = "https://api.mlab.com/api/1/databases/fhernandez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"

var clienteMLab = requestjson.createClient(urlClientes)


var urlMlabRaiz = "https://api.mlab.com/api/1/databases/fhernandez/collections/"
var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMlabRaiz
var cuentaMlabRaiz
var movimientoMlabRaiz

app.listen(port);

console.log('todo listo RESTful API server started on: ' + port);

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/', function(req, res){
  res.send("Hemos recibido su peticion POST")
})

app.put('/', function(req, res){
  res.send("Hemos recibido su peticion PUT cambiada")
})

app.delete('/', function(req, res){
  res.send("Hemos recibido su peticion DELETE")
})

app.get('/Clientes/:idcliente', function(req, res){
  res.send("Aqui tiene al cliente numero: " + req.params.idcliente)
})

app.get('/v1/Movimientos', function(req, res){
  res.sendfile('movimientosv1.json')
})

app.get('/v2/Movimientos', function(req, res){
  res.json(movimientosJSON)
})

app.get('/v2/Movimientos/:idcliente', function(req, res){
  res.send(movimientosJSON[req.params.idcliente-1])
})

app.get('/v2/movimientosquery', function(req, res){
  res.send(req.query)
})

app.post('/v2/Movimientos', function(req, res){

  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
})

app.get('/Clientes', function(req, res){

  clienteMLab.get('', function(err, resM, body){
    if(!err){
      res.send(body)
    }else{
      console.log(body);
    }
  })

})

app.post('/Clientes', function(req, res){

  clienteMLab.post('', req.body, function(err, resM, body){

    res.send(body)

  })

})

//enpoint to do login in app
app.post('/Login', function (req, res) {

  var query = 'q={"email": "'+req.body.email+'", "password": "'+req.body.password+'"}'

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios" + apiKey + "&" +query)
  clienteMlabRaiz.get('', function(err, resM, body){
    if(!err){
      if (body.length == 1) {
        const payload = {
          check:  true
        };
        const token = jwt.sign(payload, app.get('llave'), {
          expiresIn: 1440
        });
        console.log('Autentificación exitosa')
        res.status(200).json({
          mensaje: 'Autenticación correcta',
          token: token,
          user: body
        })
      }else{
        res.status(404).send("Usuario no encontrado")
      }
    }
  })

})

// Middleware to safe routes with json web token
const protectedRoutes = express.Router();
protectedRoutes.use((req, res, next) => {
    const token = req.headers['access-token'];
    console.log('Headers enviados: ' + req.headers)
    if (token) {
      jwt.verify(token, app.get('llave'), (err, decoded) => {
        if (err) {
          return res.json({
            mensaje: 'Token inválido',
            error: err
          });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      res.send({
          mensaje: 'Token no proveída.'
      });
    }
 });

//endpoint to get the accounts of a user
app.get('/Accounts/:idCostumer', protectedRoutes, function(req, res){

   var query = 'q={"email": "'+req.params.idCostumer+'"}'

   cuentaMlabRaiz = requestjson.createClient(urlMlabRaiz + "Cuentas" + apiKey + "&" +query)

   cuentaMlabRaiz.get('', function(err, resM, body){
     if(!err){
       console.log(body)
       res.send(body)
     }else{
       console.log(body)
     }
   })

 })

//endpoint to add a new movement into an account
app.put('/Accounts/:email/:numberAccount', protectedRoutes, function(req, res){

    var query = 'q={"email":"'+req.params.email+'","numberAccount":"'+req.params.numberAccount+'"}'

    movimientoMlabRaiz = requestjson.createClient(urlMlabRaiz + "Cuentas" + apiKey + "&" + query + "&" + "m=true")
    var reqBody = {$push: req.body}

    movimientoMlabRaiz.put('', reqBody, function(err, resM, body){
      if(!err){
        console.log(body)
        res.status(200).send(body)
      }else{
        console.log(body);
      }
    })

})

//enpoint to add a new user
app.post('/Users', function (req, res) {

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios" + apiKey)

  clienteMlabRaiz.post('', req.body, function(err, resM, body){
    if(!err){
      if (body != null) {
        res.status(200).send("Usuario registrado")
      }
    }
  })

})

//enpoint to add a new account
app.post('/Accounts', protectedRoutes, function (req, res) {

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Cuentas" + apiKey)

  clienteMlabRaiz.post('', req.body, function(err, resM, body){
    if(!err){
      if (body != null) {
        res.status(200).send("Cuenta registrada")
      }
    }
  })

})
